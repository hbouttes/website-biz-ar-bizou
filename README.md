# Website Biz ar Bizou

Site web de la bijouterie Biz ar Bizou

## Available Scripts

Dans le dossier racine du projet, vous pouvez exécuter:

### `npm install`

Installe toutes les dépendances du projet. 

### `npm run start`

Pour lancer le serveur métro.

### `npm run android`

Pour lancer l'application sur Android.
Vous avez besoin d'un appareil android connecté à votre ordinateur ou d'un émulateur android sur lequel installer l'application.
Pour plus de détail: [Mise en place de l'environement](https://reactnative.dev/docs/environment-setup)

### `npm run web`

Pour lancer l'application sur le navigateur. 
